package us.theappacademy.redditreader;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class RedditPostAdapter extends RecyclerView.Adapter<RedditPostHolder>{

    private ArrayList<RedditPost> redditPosts;

    public RedditPostAdapter(ArrayList<RedditPost> redditPosts){
        this.redditPosts = redditPosts;
    }

    @Override
    public RedditPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new RedditPostHolder(view);
    }

    @Override
    public void onBindViewHolder(RedditPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("RedditPostAdapter", "Title onClick: " + redditPosts.get(position).title);
            }
        });

        holder.titleText.setText(redditPosts.get(position).title);
    }

    @Override
    public int getItemCount() {
        return redditPosts.size();
    }
}
